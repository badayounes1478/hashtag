const express = require('express')
const logger = require("../logger")
const router = express.Router()

const appStore = require('../Schema/AppStore')
router.post('/insertUserToken', async (req, res) => {

    try {
        if (req.body !== null) {
            const userDataOfAppStore = await appStore.create(req.body)

            return res.status(200).json({
                message: "user token added into app store...",
                appStoreUserData: userDataOfAppStore
            })
        }
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: "internal server error"
        })
    }
})

router.post('/getAppToken',async(req,res)=>{
    try{
        if(req.body!==null){
            const appstoreData= await appStore.find({userId:req.body.userId,appType:req.body.appType})
            if(appstoreData.length){
                return res.status(200).json({
                    message:"success",
                    appStoreUserData:appstoreData
                }) 
            }else{
                return res.status(404).json({
                    message:"not found",
                    appStoreUserData:appstoreData
                })
            }
        }
    }catch(error){
        console.log(error)
        res.status(500).json({
            error: "internal server error"
        })  
    }
})

module.exports = router