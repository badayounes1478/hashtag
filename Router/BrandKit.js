const express = require('express');
const router = express.Router();
const brandkit = require('../Schema/BrandKit')
const { generateuploadURLToS3, deleteOldFile } = require('./S3.js');
const logger = require("../logger")

//@Route brandkit/generate/url
router.get('/generate/url', async (req, res) => {
    try {
        const url = await generateuploadURLToS3()
        res.send(url)
    } catch (error) {
        res.status(500).json({
            error: "Internal server error"
        })
    }
})

//@Route brandkit/create
router.post("/create", async (req, res) => {
    try {
        const data = await brandkit.create(req.body)
        res.send(data)
    } catch (error) {
        res.status(500).json({
            error: "Internal server error"
        })
    }
})


//@Route brandkit/:workspaceId
router.get('/:workspaceId', async (req, res) => {
    try {
        const data = await brandkit.find({ workspaceId: req.params.workspaceId })
        res.send(data)
    } catch (error) {
        res.status(500).json({
            error: "internal server error"
        })
    }
})

//@Route brandkit/:id
router.put('/:id', async (req, res) => {
    try {
        let data = await brandkit.findById(req.params.id)
        data.title = req.body.title
        data.logos = req.body.logos
        data.colorsData = req.body.colorsData
        data.text = req.body.text
        data = await data.save()
        res.send(data)
    } catch (error) {
        res.status(500).json({
            error: "internal server error"
        })
    }
})


//@Route brandkit/:workspaceId/:id
router.get('/:workspaceId/:id', async (req, res) => {
    try {
        const data = await brandkit.findOne({ workspaceId: req.params.workspaceId, _id: req.params.id })
        res.send(data)
    } catch (error) {
        res.status(500).json({
            error: "internal server error"
        })
    }
})



router.post("/remove/file", async (req, res) => {
    try {
        await deleteOldFile(req.body.fileKey)

        let data = "file deleted"
        if (req.body.editId !== null) {
            data = await brandkit.findById(req.body.editId)
            data.logos = req.body.logos
            data = await data.save()
        }

        res.send(data)
    } catch (error) {
        res.status(500).json({
            error: "internal server error"
        })
    }
})

module.exports = router