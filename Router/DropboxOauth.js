const express = require('express')
const axios = require('axios')
const router = express.Router()
const logger = require("../logger")

const config = {
  axios,
  clientId: '7ww9q058sy1p9ae',
  clientSecret: 'hyla8b7624zxop7',

};
const { Dropbox } = require('dropbox');

const dbx = new Dropbox(config);

const redirectUri = `https://6cgnepgh03.execute-api.ap-south-1.amazonaws.com/latest/auth`;

router.get('/', (req, res) => {
  try {
    dbx.auth.getAuthenticationUrl(redirectUri, null, 'code', 'offline', null, 'none', false)
      .then((authUrl) => {
        res.writeHead(302, { Location: authUrl });
        res.end();
      });
  } catch (error) {
    logger.error(error)
    res.status(500).json({
      error: "internal server error"
    })
  }
});


module.exports = router;