const S3 = require('aws-sdk/clients/s3')
const { v1: uuidv1 } = require('uuid');
const bucketName = "cretosaurus-brand-kit"
const region = "ap-south-1"
const accessKeyId = "AKIAQ5RO3HOR4GBP4NUC"
const secretAccessKey = "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"
const logger = require("../logger")
const s3 = new S3({
  region,
  accessKeyId,
  secretAccessKey
})

//upload a file to s3
const generateuploadURLToS3 = async () => {
  try {
    const imageName = uuidv1()
    const expireSecondsOfURL = 60 * 30

    const params = ({
      Bucket: bucketName,
      Key: imageName,
      Expires: expireSecondsOfURL,
    })

    const uploadURL = await s3.getSignedUrlPromise('putObject', params)
    return { uploadURL, imageName }
  } catch (error) {
    logger.error(error)
  }
}

const deleteOldFile = async (key) => {
  try {
    const params = { Bucket: bucketName, Key: key };
    await s3.deleteObject(params).promise();
    return "deleted";
  } catch (error) {
    return "error";
  }
};

module.exports = {
  generateuploadURLToS3,
  deleteOldFile
}