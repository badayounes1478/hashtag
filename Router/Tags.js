const express = require('express')
const tags = require('../Schema/TagsSchema')
const groupNames = require('../Schema/GroupNameSchema')
const categoryName = require('../Schema/CategorySchema')
const axios = require('axios')
const router = express.Router()
const logger = require("../logger")
const checkAuth = require('../function/checkAuth')

router.get('/:id', async (req, res) => {
    try {
        const limit = parseInt(req.query.limit) || 10; // default limit is 10
        const page = parseInt(req.query.page) || 1; // default page is 1
        const skip = (page - 1) * limit;

        const recivedTags = await tags.find({ organizationId: req.params.id })
            .sort({ 'updatedAt': -1 })
            .skip(skip)
            .limit(limit);

        res.send(recivedTags)
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: "internal server error"
        })
    }
})


router.get('/title/:id', async (req, res) => {
    try {
        const recivedTags = await tags.find({ organizationId: req.params.id }, { groupName: 1 }).sort({ 'updatedAt': -1 })
        res.send(recivedTags)
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: "internal server error"
        })
    }
})

router.post('/title/update', async (req, res) => {
    try {
        const tagsData = await tags.findById(req.body.id)
        tagsData.tags.push(req.body.tag)
        await tagsData.save()
        res.send("updated the collection")
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: "internal server error"
        })
    }
})

router.get('/groupes/:id', async (req, res) => {
    try {
        const data = await groupNames.findOne({ organizationId: req.params.id })
        if (data === null) {
            res.status(200).json({
                data: 0
            })
        } else {
            let length = data.groupNames.length
            res.status(200).json({
                data: length
            })
        }
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: "internal server error"
        })
    }
})

router.put('/', async (req, res) => {
    try {
        const groupNameData = await groupNames.findOne({ userId: req.body.organizationId })
        const tagsData = await tags.findById(req.body.id)

        if (groupNameData === null) {
            const groupNameObject = {
                organizationId: req.body.organizationId,
                userId: req.body.userId,
                groupNames: [req.body.groupName]
            }

            await groupNames.create(groupNameObject)
        } else {
            if (!groupNameData.groupNames.includes(req.body.groupName)) {
                groupNameData.groupNames = [...groupNameData.groupNames, req.body.groupName]
                await groupNameData.save()
            }
        }

        tagsData.userName = req.body.userName
        tagsData.tags = req.body.tags
        tagsData.groupName = req.body.groupName
        tagsData.category = req.body.category

        await tagsData.save()

        res.status(201).json({
            message: "created"
        })
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: "internal server error"
        })
    }
})

router.post('/', async (req, res) => {
    try {
        const groupNameData = await groupNames.findOne({ userId: req.body.organizationId })

        if (groupNameData === null) {
            const groupNameObject = {
                organizationId: req.body.organizationId,
                userId: req.body.userId,
                groupNames: [req.body.groupName]
            }

            await groupNames.create(groupNameObject)
        } else {
            if (!groupNameData.groupNames.includes(req.body.groupName)) {
                groupNameData.groupNames = [...groupNameData.groupNames, req.body.groupName]
                await groupNameData.save()
            }
        }

        let data = await tags.create(req.body)
        res.status(201).json({
            message: "created",
            data: data
        })
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: "internal server error"
        })
    }
})

router.delete('/:id', async (req, res) => {
    try {
        await tags.deleteOne({ _id: req.params.id })
        res.status(200).json({
            message: "deleted"
        })
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: "internal server error"
        })
    }
})



router.post("/category/create", async (req, res) => {
    try {
        const data = await categoryName.find({ organizationId: req.body.organizationId, category: req.body.category })
        if (data.length === 0) {
            let createdData = await categoryName.create(req.body)
            res.status(201).json({
                message: "created",
                data: createdData
            })
        } else {
            res.status(200).json({
                message: "all ready there"
            })
        }
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: "internal server error"
        })
    }
})

router.get("/category/:id", async (req, res) => {
    try {
        const data = await categoryName.find({ organizationId: req.params.id })
        res.send(data)
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: "internal server error"
        })
    }
})


router.get("/tags/search", checkAuth, async (req, res) => {
    try {
        let allowData = ["60c2087a66b6965311ea5951", "60c2097766b6965311ea5956", "62396355bd67be0b54c652ce", "624d77357ec3dda91d7ca433"]
        if (allowData.includes(req.body.userId) === false) return res.status(400).json({ error: 'Contact us to get access' })
        const tags = await axios.get(`https://api.ritekit.com/v1/stats/hashtag-suggestions?text=${req.query.query}&client_id=1d2ec4f12dbe62819b3e1256bbeb7d211bd8c5aa3a51`)
        res.send(tags.data.data)
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: "internal server error"
        })
    }
})

module.exports = router