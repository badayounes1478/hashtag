const flickrRoutes = require("express").Router();
const passport = require("passport");
const flickrToken = require('../Schema/flickrToken');
const Flickr = require('flickr-sdk');
const axios = require('axios');
const logger = require("../logger")


flickrRoutes.get("/login",

  passport.authenticate("flickr"));

// redirect url 
flickrRoutes.get(
  "/redirect",
  passport.authenticate("flickr"),
  async (req, res) => {
    try {
      const authToken = req.user.oauthToken;
      const authTokenSecret = req.user.oauthTokenSecret;
      const user_id = req.user.profile.id;

      await flickrToken.create({
        authToken,
        authTokenSecret,
        user_id
      })

      res.send("<script>window.close()</script>");

    } catch (error) {
      logger.error(error)
    }
  }
);


// check url if auth token is there or not 
flickrRoutes.get("/check", async (req, res) => {
  const token = await flickrToken.find();
  if (token) {
    await axios.get("https://localhost:4008/flickr/get").then((data) => {
      res.send(data.data);
    }).catch((error) => {
      logger.error(error)
      res.send("error");
    })
  } else {
    res.send("error");
  }
})



// getting data using token 
flickrRoutes.get("/get", async (req, res) => {
  const token = await flickrToken.find();
  const l = token.length;
  if (l == 0) {
    res.send("error");
    res.end();
  } else {
    const auth_token = await token[0].authToken;
    const token_secret = await token[0].authTokenSecret;
    const user_id = await token[0].userId;
    let flickr = new Flickr(
      Flickr.OAuth.createPlugin(
        process.env.FLICKR_CONSUMER_KEY,
        process.env.FLICKR_CONSUMER_SECRET,
        auth_token,
        token_secret
      )
    );
    // photo url format : { https://live.staticflickr.com/{server-id}/{id}_{secret}.jpg }
    const photo_url_base = "https://live.staticflickr.com";
    flickr.people
      .getPhotos({
        user_id: user_id,
      })
      .then((re) => {
        let photos_data = re.body.photos.photo;
        let photo_urls = [];
        photos_data.forEach(e => {
          let server_id = e.server;
          let id = e.id;
          let secret = e.secret;
          let final_photo_url = `${photo_url_base}/${server_id}/${id}_${secret}.jpg`;
          photo_urls.push(final_photo_url);
        });
        res.send(photo_urls);
      })
      .catch((error) => {
        logger.error(error)
        res.end();
      });
  }
});

module.exports = flickrRoutes;
