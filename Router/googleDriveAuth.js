const express = require('express');
const { loginGoogleDrive, callback, middleWare, logOut, getAccessToken, getUsersFiles } = require('../function/googleDriveAuth');
const router = express.Router();

const storeTheUserIdInSession = (req, res, next) => {
    req.session.userId = req.params.userId;
    next();
};

router.get("/login/:userId", storeTheUserIdInSession, loginGoogleDrive);

router.get("/auth/callback", middleWare, callback);

router.get("/logout/:userId", logOut);

router.get("/get/access/token/:userId", getAccessToken);

module.exports = router;