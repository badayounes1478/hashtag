const mongoose = require('mongoose')
const schema = mongoose.Schema

const groupName = new schema({
    organizationId: String,
    userId: String,
    groupNames: [String]
}, { timestamps: true })

module.exports = mongoose.model("Group Name", groupName)
