const mongoose = require('mongoose')
const schema = mongoose.Schema;

const Qrcode = new schema({
    userId:{
        type:String
    },
    title:{
        type:String
    },
    qrcodeContent:{
        type:String
    },
    color:{
        type:String
    },
    qrcode:{
        type:String
    }
}) 

module.exports = mongoose.model('Qrcode',Qrcode)