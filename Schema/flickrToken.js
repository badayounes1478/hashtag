const mongoose = require('mongoose');

const TokenSchema = new mongoose.Schema({
    authToken : {
        type : String
    },
    authTokenSecret : {
        type : String
    },
    userId :{
        type : String
    }
});

const tokenFlickr = mongoose.model("tokenFlickr",TokenSchema);
module.exports = tokenFlickr;