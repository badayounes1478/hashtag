const mongoose = require('mongoose');
const schema = mongoose.Schema;

const googleSchema = new schema({
    userId: {
        type: String
    },
    access_token: {
        type: String
    },
    refresh_token: {
        type: String
    },
    token_active: {
        type: Boolean
    },
    app_type: {
        type: String
    }
});

module.exports = mongoose.model('GoogleSchema', googleSchema);