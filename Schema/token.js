const mongoose = require('mongoose');

const TokenSchema = new mongoose.Schema({
    accessToken : {
        type : String
    },
    refreshToken : {
        type : String
    },
    userId:{
        type:String
    }
});

const token = mongoose.model("token",TokenSchema);
module.exports = token;