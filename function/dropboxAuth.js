const axios = require('axios')
const logger = require("../logger")

const config = {
  axios,
  clientId: '7ww9q058sy1p9ae',
  clientSecret: 'hyla8b7624zxop7',
};
const { Dropbox } = require('dropbox');

const dbx = new Dropbox(config);

const redirectUri = `https://6cgnepgh03.execute-api.ap-south-1.amazonaws.com/latest/auth`;

const dropboxAuth = async (req, res) => {
  try {
    const { code } = req.query;

    dbx.auth.getAccessTokenFromCode(redirectUri, code)
      .then((token) => {
        dbx.auth.setRefreshToken(token.result.refresh_token);
        res.redirect(`https://www.cache.creatosaurus.io/home?token=${token.result.access_token}`)

      })
      .catch((error) => {
        logger.error(error)
      });
  } catch (error) {
    logger.error(error)
    res.status(500).json({
      error: "internal server error"
    })
  }
}

module.exports = {
  dropboxAuth
}