const { google } = require('googleapis');
const GoogleSchema = require("../Schema/googleSchema");
const axios = require("axios");

const oauth2Client = new google.auth.OAuth2(
    "148537948078-v9ofeuph3fe7gq8t9o9obi8d5odg080c.apps.googleusercontent.com",
    "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",
    "https://6cgnepgh03.execute-api.ap-south-1.amazonaws.com/latest/google/drive/auth/callback"
);


exports.loginGoogleDrive = async (req, res) => {
    try {
        if (req.params.userId !== undefined) {
            const authorizationUrl = oauth2Client.generateAuthUrl({
                access_type: 'offline',
                scope: 'https://www.googleapis.com/auth/drive.readonly',
                include_granted_scopes: true
            });
            res.redirect(authorizationUrl);
        }
    } catch (error) {
        res.status(500).json({
            error: "Internal Server Error"
        });
    }
};


exports.middleWare = async (req, res, next) => {
    try {
        if (req.query.code !== undefined) {
            let { tokens } = await oauth2Client.getToken(req.query.code);
            const isUserThere = await GoogleSchema.findOne({ userId: req.session.userId, app_type: "google_drive" });
            if (isUserThere === null) {
                let user = {
                    userId: req.session.userId,
                    access_token: tokens.access_token,
                    refresh_token: tokens.refresh_token,
                    token_active: true,
                    app_type: "google_drive"
                };
                await GoogleSchema.create(user);
                req.user = user;
            }
        };
    } catch (error) {
        res.status(500).json({
            error: "Internal Server Error"
        });
    }
    next();
};

exports.callback = async (req, res) => {
    try {
        if (req.user === null) return res.status(500).json({ error: 'Auth failed' });
        res.redirect(`https://www.cache.creatosaurus.io/auth?code=appstore&type=appstore&app=googledrive`);
    } catch (error) {
        res.status(500).json({
            error: "Internal Server Error"
        });
    }
};


exports.logOut = async (req, res) => {
    try {
        const user = await GoogleSchema.findOneAndDelete({ userId: req.params.userId, app_type: "google_drive" });
        if (user !== null) {
            const response = await axios.post(` https://oauth2.googleapis.com/revoke?token=${user.access_token}`, {
                header: {
                    'Content-type': 'application/x-www-form-urlencoded'
                }
            });
            if (response.status === 200) {
                res.status(200).json({
                    data: "User logout successfully"
                });
            }
        }
    } catch (error) {
        res.status(500).json({
            error: "Internal Server Error"
        });
    }
};


exports.getAccessToken = async (req, res) => {
    try {
        if (req.params.userId !== undefined) {
            const data = await GoogleSchema.findOne({ userId: req.params.userId, app_type: "google_drive" });
            if (data !== null) {
                res.status(200).json({
                    access_token: data.access_token
                });
            } else {
                res.status(200).json({
                    message: "drive not connected"
                });
            }
        }
    } catch (error) {
        res.status(500).json({
            error: "Internal Server Error"
        });
    }
};


