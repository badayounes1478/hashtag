const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const session = require("express-session");
const snapRoutes = require('./Router/snap-routes');
const app = express();
const logger = require("./logger");
require("dotenv").config();

const passportSetup = require('./passport-setup');
const {
  dropboxAuth
} = require('./function/dropboxAuth');

mongoose.connect('mongodb+srv://creatosaurus1:EOaVFfQ5YhOD3UhF@creatosaurus.7trc5.mongodb.net/hashtag', { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => logger.info("hashtag database connected"))
  .catch(error => logger.error(error));


app.use(express.json());
app.use(cors());
app.use(
  session({
    resave: false,
    saveUninitialized: true,
    secret: "bla bla bla",
  })
);

app.use('/hashtag', require('./Router/Tags'));
app.use('/dropbox', require('./Router/DropboxOauth'));
app.use('/appstore', require('./Router/AppStore'));
app.use("/snapchat", snapRoutes);
app.use("/flickr", require('./Router/flickr-routes'));
app.use('/brandkit', require('./Router/BrandKit'));
app.use('/qrcode', require('./Router/Qrcode'));

//auth for dropbox...
app.get('/auth', dropboxAuth);



module.exports = app;