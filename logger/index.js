const productionLogger = require("./productionLogger");
const developmentLogger = require("./developementLogger");

let logger = null;

if (process.env.NODE_ENV === "production") {
  logger = productionLogger();
} else {
  logger = developmentLogger();
}

module.exports = logger;
