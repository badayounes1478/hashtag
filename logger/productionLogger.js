const { createLogger, format, transports } = require('winston');
require('winston-mongodb');

const productionLogger = () => {
  return createLogger({
    format: format.combine(
      format.timestamp(),
      format.json(),
      format.metadata()
    ),
    transports: [
      new transports.Console(),
      new transports.MongoDB({
        level: 'info',
        db: 'mongodb+srv://creatosaurus1:EOaVFfQ5YhOD3UhF@creatosaurus.7trc5.mongodb.net/hashtag',
        options: {
          useUnifiedTopology: true,
        },
      }),
    ],
  });
};

module.exports = productionLogger;
