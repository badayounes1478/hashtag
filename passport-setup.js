const passport = require("passport");
const SnapchatStrategy = require("passport-snapchat").Strategy;
const FlickerStrategy = require("passport-flickr").Strategy;
passport.use(
  new SnapchatStrategy(
    {
      clientID: process.env.clientID,
      clientSecret: process.env.clientSecret,
      callbackURL: "https://6cgnepgh03.execute-api.ap-south-1.amazonaws.com/latest/snapchat/redirect",
      scope: ["user.display_name", "user.bitmoji.avatar"],
      passReqToCallback : true
    },
    function(req,accessToken, refreshToken, profile, done){
      done(null, {userId:req.query.state,accessToken: accessToken, refreshToken: refreshToken });
    }
  )
);
passport.use(
  new FlickerStrategy(
    {
      consumerKey: process.env.FLICKR_CONSUMER_KEY,
      consumerSecret: process.env.FLICKR_CONSUMER_SECRET,
      callbackURL: "https://localhost:4008/flickr/redirect",
    },
    function(token, tokenSecret, profile, done) {
      done(null,{oauthToken : token,oauthTokenSecret : tokenSecret,profile : profile});
    }
  )
);


passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((user, done) => {
  done(null, user);
});
